Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :devices, only: [:index, :create]
      resources :missing, only: [:index, :create, :destroy] do
        resources :reports, only: [:index, :create]
      end
    end
  end
end
