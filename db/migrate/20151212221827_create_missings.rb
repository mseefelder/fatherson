class CreateMissings < ActiveRecord::Migration
  def change
    create_table :missings do |t|
      t.string :identifier

      t.timestamps null: false
    end
  end
end
