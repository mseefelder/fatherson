class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.references :missing, index: true, foreign_key: true
      t.references :device, index: true, foreign_key: true
      t.integer :signal

      t.timestamps null: false
    end
  end
end
