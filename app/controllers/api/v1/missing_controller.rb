class Api::V1::MissingController < Api::BaseController
	def index
		@missings = Missing.all
		render json: { status: :success, data: @missings }
	end

	def create
		@missing = Missing.new(missing_params)
		if @missing.save
			render json: { status: :success, data: @missing}
		else
			render json: { status: :fail, data: @missing.errors.messages }
		end
	end

	def destroy
		@missing = Missing.find(params[:id])
		if @missing.destroy
			render json: { status: :success }
		else
			render json: { status: :fail }
		end
	end

	private

	def missing_params
		params.require(:missing).permit(:identifier)
	end
end