class Api::V1::DevicesController < Api::BaseController
	def index
		@devices = Device.all
		render json: {status: :success, data: @devices}
	end

	def create
		@device = Device.new(device_params)
		if @device.save
			render json: {status: :success, data: @device}
		else
			render json: {status: :fail, data: @device.errors.messages}
		end
	end

	def device_params
		params.require(:device).permit(:lon, :lat, :metadata)
	end
end