class Api::V1::ReportsController < Api::BaseController
	def index
		@missing = Missing.find params[:missing_id]
		render json: {status: :success, data: @missing.reports}
	end

	def create
		@missing = Missing.find params[:missing_id]
		@report = @missing.reports.build(report_params)
		if @report.save
			render json: {status: :success, data: @report}
		else
			render json: {status: :fail, data: @report.errors.messages}
		end
	end

	def report_params
		params.require(:report).permit(:signal, :device_id)
	end
end